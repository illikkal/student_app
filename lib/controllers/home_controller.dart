import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:student_app/models/enquiry.dart';
import 'package:student_app/models/student.dart';

class HomeController extends GetxController with SingleGetTickerProviderMixin {
  var enquiryList = <Enquiry>[].obs;
  var studentList = <Student>[].obs;
  var isLoading = false.obs;
  TabController? tabController;
  final List<String> titleList = [
    "List of Enquiries",
    "List of students",
  ];
  var title = "List of Enquiries".obs;
  @override
  void onInit() {
    getEnquiries();
    getStudents();
    tabController = TabController(length: 2, vsync: this);
    tabController?.addListener(() {
      title.value = titleList[tabController?.index ?? 0];
    });
    super.onInit();
  }

  @override
  void onClose() {
    tabController?.dispose();
    super.onClose();
  }

  void changeTitle() {}

  void getEnquiries() async {
    isLoading.value = true;
    final response = await http
        .get(Uri.parse("http://www.mocky.io/v2/5c41920e0f0000543fe7b889"));
    if (response.statusCode == 200) {
      var responseBody = json.decode(response.body);
      //debugPrint(responseBody.toString());
      var enquiries = responseBody["dataList"]
          .map<Enquiry>((item) => Enquiry.fromMap(item))
          .toList();
      enquiryList.assignAll(enquiries);
    } else {
      Get.snackbar("Error", "Error occured !, please try later");
    }
    isLoading.value = false;
  }

  void getStudents() async {
    isLoading.value = true;
    final response = await http
        .get(Uri.parse("http://www.mocky.io/v2/5c41950b0f0000543fe7b8a2"));
    if (response.statusCode == 200) {
      var responseBody = json.decode(response.body);
      debugPrint(responseBody.toString());
      var students = responseBody["dataList"]
          .map<Student>((item) => Student.fromMap(item))
          .toList();
      studentList.assignAll(students);
    } else {
      Get.snackbar("Error", "Error occured !, please try later");
    }
    isLoading.value = false;
  }
}
