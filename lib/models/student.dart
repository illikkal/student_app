class Student {
  String batchName = "";
  String studentType = "";
  String phoneNumber = "";
  bool isArchived = false;
  String created = "";
  String name = "";
  String platformTag = "";
  bool isRead = false;
  int enquiryId = 0;
  int attendeeId = 0;
  String category = "";
  int categoryId = 0;
  Student.fromMap(dynamic obj) {
    this.batchName = obj["batchName"] ?? "";
    this.studentType = obj["studentType"] ?? "";
    this.phoneNumber = obj["phoneNumber"] ?? "";
    this.platformTag = obj["platformTag"] ?? "";
    this.isArchived = obj["isArchived"] ?? false;
    this.created = obj["created"] ?? "";
    this.name = obj["name"] ?? "";
    this.isRead = obj["isRead"] ?? false;
    this.enquiryId = obj["enquiryId"] ?? 0;
    this.attendeeId = obj["attendeeId"] ?? 0;
    this.category = obj["category"] ?? "";
    this.categoryId = obj["categoryId"] ?? 0;
  }
}
