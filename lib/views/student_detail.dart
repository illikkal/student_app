import 'package:flutter/material.dart';
import 'package:student_app/models/student.dart';
import 'dart:math';
import 'package:url_launcher/url_launcher.dart';

class StudentDetail extends StatelessWidget {
  final Student student;
  StudentDetail(this.student);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("List of Students"),
        backgroundColor: Colors.green,
      ),
      body: Container(
        //width: Get.width,
        margin: EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            CircleAvatar(
              backgroundColor:
                  Colors.primaries[Random().nextInt(Colors.primaries.length)],
              child: Text(
                student.name.substring(0, 1),
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              //width: 100,
                              child: Text(
                                student.name.length > 15
                                    ? student.name.substring(0, 15)
                                    : student.name,
                                style: TextStyle(fontWeight: FontWeight.w700),
                                overflow: TextOverflow.fade,
                              ),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              student.platformTag,
                              style: TextStyle(
                                  color: Colors.orange,
                                  fontWeight: FontWeight.w600),
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 15.0),
                              child: Text(
                                student.created,
                                style: TextStyle(fontSize: 12),
                              ),
                            ),
                            IconButton(
                              padding: EdgeInsets.zero,
                              constraints: BoxConstraints(),
                              onPressed: () {},
                              icon: Icon(Icons.more_vert),
                            ),
                          ],
                        )
                      ],
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Text(
                      student.category,
                      style: TextStyle(fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          student.batchName,
                          style: TextStyle(fontWeight: FontWeight.w500),
                        ),
                        InkWell(
                          onTap: () async {
                            String url = 'tel://' + student.phoneNumber;
                            debugPrint(url + "ph no");
                            if (await canLaunch(url)) {
                              await launch(url);
                            } else {
                              throw 'Could not launch $url';
                            }
                          },
                          child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(2),
                                color: Colors.green,
                              ),
                              margin: EdgeInsets.only(right: 4),
                              padding: EdgeInsets.all(3),
                              child: Icon(
                                Icons.phone,
                                color: Colors.white,
                                size: 10,
                              )),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 4,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
