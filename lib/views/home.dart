import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:student_app/controllers/home_controller.dart';
import 'package:student_app/models/student.dart';
import 'package:student_app/views/enquiry_detail.dart';
import 'package:student_app/views/student_detail.dart';
import 'dart:math';
import 'package:url_launcher/url_launcher.dart';
import 'package:student_app/models/enquiry.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({Key? key}) : super(key: key);
  final HomeController _controller = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Scaffold(
        body: _controller.isLoading.value
            ? Center(child: CircularProgressIndicator())
            : Scaffold(
                appBar: AppBar(
                  title: Obx(() => Text(_controller.title.value)),
                  backgroundColor: Colors.green,
                ),
                body: Column(
                  children: [
                    TabBar(
                      controller: _controller.tabController,
                      labelColor: Colors.black,
                      indicatorColor: Colors.green,
                      tabs: [
                        Tab(
                          text: "Enquiries",
                        ),
                        Tab(
                          text: "Students",
                        ),
                      ],
                    ),
                    Expanded(
                      child: TabBarView(
                        controller: _controller.tabController,
                        children: [
                          //enquiries builder
                          ListView.builder(
                              shrinkWrap: true,
                              itemCount: _controller.enquiryList.length,
                              itemBuilder: (context, index) =>
                                  enquiry(_controller.enquiryList[index])),

                          //students builder
                          Container(
                            child: ListView.builder(
                                shrinkWrap: true,
                                itemCount: _controller.studentList.length,
                                itemBuilder: (context, index) => studentTile(
                                    _controller.studentList[index])),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }

  Widget enquiry(Enquiry enquiry) {
    return InkWell(
      onTap: () {
        Get.to(() => EnquiryDetail(enquiry));
      },
      child: Container(
        //width: Get.width,
        margin: EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            CircleAvatar(
              backgroundColor:
                  Colors.primaries[Random().nextInt(Colors.primaries.length)],
              child: Text(
                enquiry.name.substring(0, 1),
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              //width: 100,
                              child: Text(
                                enquiry.name.length > 15
                                    ? enquiry.name.substring(0, 15)
                                    : enquiry.name,
                                style: TextStyle(fontWeight: FontWeight.w700),
                                overflow: TextOverflow.fade,
                              ),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              "new",
                              style: TextStyle(color: Colors.orange),
                            )
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 15.0),
                          child: Text(
                            enquiry.createdOn,
                            style: TextStyle(fontSize: 12),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          enquiry.categoryName,
                          style: TextStyle(fontWeight: FontWeight.w500),
                        ),
                        InkWell(
                          onTap: () async {
                            String url = 'tel://' + enquiry.phoneNumber;
                            debugPrint(url + "ph no");
                            if (await canLaunch(url)) {
                              await launch(url);
                            } else {
                              throw 'Could not launch $url';
                            }
                          },
                          child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(2),
                                color: Colors.green,
                              ),
                              margin: EdgeInsets.only(right: 4),
                              padding: EdgeInsets.all(3),
                              child: Icon(
                                Icons.phone,
                                color: Colors.white,
                                size: 10,
                              )),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Text(
                      enquiry.location,
                      style: TextStyle(fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Row(
                      children: [
                        Text(
                          'At ${enquiry.classLocPref}',
                          style: TextStyle(fontWeight: FontWeight.w500),
                        ),
                        Icon(
                          enquiry.classLocPref == "HOME"
                              ? Icons.home
                              : Icons.business_outlined,
                          size: 16,
                        )
                      ],
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          enquiry.tag,
                          style: TextStyle(fontWeight: FontWeight.w500),
                        ),
                        StatefulBuilder(builder:
                            (BuildContext context, StateSetter setState) {
                          return IconButton(
                              padding: EdgeInsets.zero,
                              constraints: BoxConstraints(),
                              onPressed: () {
                                enquiry.isStarred = !enquiry.isStarred;
                                setState(() {});
                              },
                              icon: enquiry.isStarred
                                  ? Icon(
                                      Icons.star,
                                      color: Colors.yellow[700],
                                    )
                                  : Icon(Icons.star_border));
                        })
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget studentTile(Student student) {
    return InkWell(
      onTap: () => Get.to(() => StudentDetail(student)),
      child: Container(
        //width: Get.width,
        margin: EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            CircleAvatar(
              backgroundColor:
                  Colors.primaries[Random().nextInt(Colors.primaries.length)],
              child: Text(
                student.name.substring(0, 1),
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              //width: 100,
                              child: Text(
                                student.name.length > 15
                                    ? student.name.substring(0, 15)
                                    : student.name,
                                style: TextStyle(fontWeight: FontWeight.w700),
                                overflow: TextOverflow.fade,
                              ),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              student.platformTag,
                              style: TextStyle(
                                  color: Colors.orange,
                                  fontWeight: FontWeight.w600),
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 15.0),
                              child: Text(
                                student.created,
                                style: TextStyle(fontSize: 12),
                              ),
                            ),
                            IconButton(
                              padding: EdgeInsets.zero,
                              constraints: BoxConstraints(),
                              onPressed: () {},
                              icon: Icon(Icons.more_vert),
                            ),
                          ],
                        )
                      ],
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Text(
                      student.category,
                      style: TextStyle(fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          student.batchName,
                          style: TextStyle(fontWeight: FontWeight.w500),
                        ),
                        InkWell(
                          onTap: () async {
                            String url = 'tel://' + student.phoneNumber;
                            debugPrint(url + "ph no");
                            if (await canLaunch(url)) {
                              await launch(url);
                            } else {
                              throw 'Could not launch $url';
                            }
                          },
                          child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(2),
                                color: Colors.green,
                              ),
                              margin: EdgeInsets.only(right: 4),
                              padding: EdgeInsets.all(3),
                              child: Icon(
                                Icons.phone,
                                color: Colors.white,
                                size: 10,
                              )),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 4,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
