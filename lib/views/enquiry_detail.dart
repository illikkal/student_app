import 'package:flutter/material.dart';
import 'package:student_app/models/enquiry.dart';
import 'dart:math';
import 'package:url_launcher/url_launcher.dart';

class EnquiryDetail extends StatelessWidget {
  final Enquiry enquiry;
  EnquiryDetail(this.enquiry);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("List of Enquiries"),
        backgroundColor: Colors.green,
      ),
      body: Container(
        //width: Get.width,
        margin: EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            CircleAvatar(
              backgroundColor:
                  Colors.primaries[Random().nextInt(Colors.primaries.length)],
              child: Text(
                enquiry.name.substring(0, 1),
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              //width: 100,
                              child: Text(
                                enquiry.name.length > 15
                                    ? enquiry.name.substring(0, 15)
                                    : enquiry.name,
                                style: TextStyle(fontWeight: FontWeight.w700),
                                overflow: TextOverflow.fade,
                              ),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              "new",
                              style: TextStyle(color: Colors.orange),
                            )
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 15.0),
                          child: Text(
                            enquiry.createdOn,
                            style: TextStyle(fontSize: 12),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          enquiry.categoryName,
                          style: TextStyle(fontWeight: FontWeight.w500),
                        ),
                        InkWell(
                          onTap: () async {
                            String url = 'tel://' + enquiry.phoneNumber;
                            debugPrint(url + "ph no");
                            if (await canLaunch(url)) {
                              await launch(url);
                            } else {
                              throw 'Could not launch $url';
                            }
                          },
                          child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(2),
                                color: Colors.green,
                              ),
                              margin: EdgeInsets.only(right: 4),
                              padding: EdgeInsets.all(3),
                              child: Icon(
                                Icons.phone,
                                color: Colors.white,
                                size: 10,
                              )),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Text(
                      enquiry.location,
                      style: TextStyle(fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Row(
                      children: [
                        Text(
                          'At ${enquiry.classLocPref}',
                          style: TextStyle(fontWeight: FontWeight.w500),
                        ),
                        Icon(
                          enquiry.classLocPref == "HOME"
                              ? Icons.home
                              : Icons.business_outlined,
                          size: 16,
                        )
                      ],
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          enquiry.tag,
                          style: TextStyle(fontWeight: FontWeight.w500),
                        ),
                        StatefulBuilder(builder:
                            (BuildContext context, StateSetter setState) {
                          return IconButton(
                              padding: EdgeInsets.zero,
                              constraints: BoxConstraints(),
                              onPressed: () {
                                enquiry.isStarred = !enquiry.isStarred;
                                setState(() {});
                              },
                              icon: enquiry.isStarred
                                  ? Icon(
                                      Icons.star,
                                      color: Colors.yellow[700],
                                    )
                                  : Icon(Icons.star_border));
                        })
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
