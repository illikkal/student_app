class Enquiry {
  bool showReportInAccurate = false;
  bool isNumberShared = false;
  int enqId = 0;
  bool isArchive = false;
  bool fromAd = false;
  String classLocPref = "";
  bool isStarred = false;
  String categoryName = "";
  String createdOn = "";
  bool isUnread = false;
  String providerType = "";
  bool isJob = false;
  bool isExclusive = false;
  String enquiryType = "";
  String enquiryStatus = "";
  String phoneNumber = "";
  bool isFree = false;
  String name = "";
  String postedOn = "";
  String location = "";
  bool isReportInaccurate = false;
  String tag = "";
  Enquiry.fromMap(dynamic obj) {
    this.showReportInAccurate = obj["showReportInAccurate"] ?? false;
    this.isNumberShared = obj["isNumberShared"] ?? false;
    this.enqId = obj["engId"] ?? 0;
    this.isArchive = obj["isArchive"] ?? false;
    this.fromAd = obj["fromAd"] ?? false;
    this.classLocPref = obj["classLocPref"] ?? "";
    this.isStarred = obj["isStarred"] ?? false;
    this.categoryName = obj["categoryName"] ?? "";
    this.createdOn = obj["createdOn"] ?? "";
    this.isUnread = obj["isUnread"] ?? false;
    this.providerType = obj["providerType"] ?? "";
    this.name = obj["name"] ?? "";
    this.phoneNumber = obj["phoneNumber"] ?? "";
    this.location = obj["location"] ?? "";
    this.postedOn = obj["postedOn"] ?? "";
    this.tag = obj["tag"] ?? "";
  }
}
